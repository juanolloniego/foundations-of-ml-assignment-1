import math
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split, KFold


def create_design_matrix(data_points, poly_degree):
    resulting_mat = np.ones(data_points.shape[0])
    for phi_value in range(1, poly_degree + 1):
        resulting_mat = np.vstack((resulting_mat, np.power(data_points, phi_value)))
    return resulting_mat.T


def gradient_loss_function(a_mat, real_y, w, lambda_param):
    return 2 / len(X) * a_mat.T.dot(a_mat.dot(w) - real_y) + 2 * lambda_param * w


def reg_gradient_descent(a_mat, real_y, initial_w, iterations, current_rate, lambda_param):
    all_ws = []
    all_residuals = []

    w = initial_w
    for i in range(iterations):
        mean_residual = np.square(real_y - a_mat.dot(w)).mean()
        all_ws.append(w)
        all_residuals.append(mean_residual)
        gradients = gradient_loss_function(a_mat, real_y, w, lambda_param)
        w = w - (current_rate * gradients)
    return w, np.asarray(all_ws), np.asarray(all_residuals)


def perform_gradient_descent(points, truth, degree, number_iterations, rates, lam):
    A = create_design_matrix(X, degree)
    random_w = np.random.randn(degree + 1)
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 6))
    ax[0].scatter(X, truth, marker='o', color='k', s=100)  # Show the original data points
    for r in rates:
        best_w, w_hist, loss_hist = reg_gradient_descent(A, truth, random_w, number_iterations, r, lam)
        ax[1].plot(range(number_iterations), np.log(loss_hist), label=r)
        y_prediction = A.dot(best_w)
        ax[0].plot(points, y_prediction, label=r)

    ax[0].plot(points, ground_truth, label='Ground Truth', color='red')
    ax[0].legend()
    ax[1].legend()
    ax[1].set_title("Gradient descent for different rates")
    ax[1].set_xlabel("Number of iterations")
    ax[1].set_ylabel("Log(Mean sum squared residuals)")
    plt.suptitle("Degree {}".format(degree))
    plt.show()


def get_best_w_analytically(design_mat, lamb, truth):
    return (np.linalg.inv(design_mat.T @ design_mat + (lamb * np.eye(design_mat.shape[1]))) @ design_mat.T).dot(truth)


def plot_get_best_w_analytically(degrees, lambdas, points, truth):
    for degree, lam in zip(degrees, lambdas):
        A = create_design_matrix(points, degree)
        best_w = (np.linalg.inv(A.T @ A + lam * np.eye(degree + 1)) @ A.T).dot(truth)
        y_prediction = A.dot(best_w)
        plt.plot(points, y_prediction, label='Degree {}'.format(degree))
        plt.legend()

    plt.plot(points, ground_truth, label='Ground Truth', color='red')
    plt.scatter(X, truth, marker='o', color='k', s=100)  # Show the original data points
    plt.show()


X = np.linspace(0, 2 * math.pi, 50)
ground_truth = np.sin(X)
y0 = ground_truth + 0.3 * np.random.normal(0, scale=1, size=np.shape(X))  # y = sin(x) + epsilon

# Find the best w
polynomials_to_try = [
    (3, 4500, [0.00001, 0.00002, 0.00003], .6),
    (4, 4500, [0.000001, 0.000002, 0.0000003], .3),
]

for deg, iters, rates, l in polynomials_to_try:
    perform_gradient_descent(X, y0, deg, iters, rates, l)

# Part 2
degree_list = [3, 4, 5, 6, 7, 12]
lambda_list = [.5, .5, .5, .5, .5, .9]
plot_get_best_w_analytically(degree_list, lambda_list, X, y0)


# Part 3

residuals = []
for deg in degree_list:
    train_X, test_X, train_Y, test_Y = train_test_split(X, y0)  # Split into test and train

    train_matrix = create_design_matrix(train_X, deg)
    test_matrix = create_design_matrix(test_X, deg)

    result_w = get_best_w_analytically(train_matrix, 0, train_Y)

    residuals.append(np.square(test_Y - test_matrix.dot(result_w)).mean())

plt.plot(np.array(degree_list), residuals)
plt.title("Performance according to complexity")
plt.xlabel("Polynomial Degree")
plt.ylabel("Mean squared error")
plt.show()


lambda_list = [i/10 for i in range(0, 11)]
residuals = []
for current_lambda in lambda_list:
    train_X, test_X, train_Y, test_Y = train_test_split(X, y0)  # Split into test and train

    train_matrix = create_design_matrix(train_X, 3)
    test_matrix = create_design_matrix(test_X, 3)

    result_w = get_best_w_analytically(train_matrix, current_lambda, train_Y)
    mse = np.square(test_Y - test_matrix.dot(result_w)).mean()
    residuals.append(mse)

plt.plot(lambda_list, residuals)
plt.title("Performance according to lambda (fixed degree 3)")
plt.xlabel("Lambda Coefficient")
plt.ylabel("MSE")
plt.show()

# Part 4 k-fold cross validation
residuals = []
kf = KFold(n_splits=10)
for train, test in kf.split(X):
    train_matrix = create_design_matrix(X[train], 3)
    test_matrix = create_design_matrix(X[test], 3)

    result_w = get_best_w_analytically(train_matrix, .6, y0[train])
    mse = np.square(y0[test] - test_matrix.dot(result_w)).mean()
    residuals.append(mse)

plt.plot(range(10), residuals)
plt.title("10-Fold cross validation")
plt.xlabel("Iteration")
plt.ylabel("MSE")
plt.show()
