import scipy as sp
import numpy as np
import pandas as pd
from sklearn import datasets
import matplotlib.pyplot as plt


# Function definitions #
def project_1d(v1, v2):
    # Project v1 onto v2
    # result = (v1 . v2) \ |v2|
    return v2.dot(v1) / np.linalg.norm(v2)


# Part 3 Iris Dataset
iris = datasets.load_iris()

df = pd.DataFrame(np.concatenate((iris.data, np.array([iris.target]).T), axis=1), columns=iris.feature_names + ['target'])
means = df.groupby('target').mean().values.reshape((3, 4, 1))

iris_data = iris.data.reshape((3, 50, 4))
covariances = np.array([np.cov(class_data.T) for class_data in iris_data])

overall_mean = np.mean(iris.data, axis=0).reshape((4, 1))

# Compute ΣB
sigma_B = np.zeros((4, 4))
for mean_vec in means:
    sigma_B += 50 * (mean_vec - overall_mean).dot((mean_vec - overall_mean).T)

print("Sb", sigma_B)
print("")

# Compute ΣW
sigma_W = 49 * np.sum(covariances, axis=0)
print("Sw", sigma_W)
print("")

# Part 1
# Generalised Eigenvalue problem
# ΣBw = λΣWw

# generalised eigv prob  Aw = lamb B w

# Best one should be the first one (descending order)
eig_vals, eig_vecs = sp.linalg.eigh(sigma_B, b=sigma_W)
eig_vecs = eig_vecs.T  # So every eig_value corresponds to a row.

print('Eigenvalues: ', eig_vals)
print('Eigenvectors: ', eig_vecs)

# Check Condition holds: (ΣB − λΣW)w = 0
print("Check condition holds:")
for idx, value in enumerate(eig_vals):
    print((sigma_B - value * sigma_W) @ eig_vecs[idx])

fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 6))
# Project onto best direction w
colours_labels = [('r', 'setosa'), ('g', 'versicolor'), ('b', 'virginica')]
best_w = eig_vecs[-1]
for idx, class_data in enumerate(iris_data):
    res = project_1d(class_data.T, best_w)
    ax[0].hist(res, color=colours_labels[idx][0], label=colours_labels[idx][1], alpha=0.4, bins=15)
ax[0].legend()
ax[0].set_title("Best direction w*")

# Project onto a different, worse w*
w2 = best_w + [0, 500, 500, 500]
for idx, class_data in enumerate(iris_data):
    res = project_1d(class_data.T, w2)
    ax[1].hist(res, color=colours_labels[idx][0], label=colours_labels[idx][1], alpha=0.4, bins=15)
ax[1].set_title("Different W direction")
ax[1].legend()

w3 = best_w + [16, 0, 0, 0]
for idx, class_data in enumerate(iris_data):
    res = project_1d(class_data.T, w3)
    ax[2].hist(res, color=colours_labels[idx][0], label=colours_labels[idx][1], alpha=0.4, bins=15)
ax[2].set_title("Different W direction")
ax[2].legend()
plt.show()
