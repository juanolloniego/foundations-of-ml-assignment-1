import math
import numpy as np
import matplotlib.pyplot as plt


# Function definitions #
def project_1d(v1, v2):
    # Project v1 onto v2
    # result = (v1 . v2) \ |v2|
    return v2.dot(v1) / np.linalg.norm(v2)


def fishers_ratio(data_a, data_b):
    mu_a = np.mean(data_a)
    sigma_a = np.std(data_a) ** 2

    mu_b = np.mean(data_b)
    sigma_b = np.std(data_b) ** 2

    count_a = len(data_a)
    count_b = len(data_b)
    total_points = count_a + count_b

    num = (mu_a - mu_b) ** 2
    den = (count_a / total_points) * sigma_a + (count_b / total_points) * sigma_b
    return num / den


def unbalanced_fishers_ratio(data_a, data_b):
    mu_a = np.mean(data_a)
    sigma_a = np.std(data_a) ** 2

    mu_b = np.mean(data_b)
    sigma_b = np.std(data_b) ** 2

    num = (mu_a - mu_b) ** 2
    den = sigma_a + sigma_b
    return num / den


def gaussian_2d(pos, cov, mean):
    norm_const = 2 * math.pi * np.sqrt(np.linalg.det(cov))
    # The np.einsum calculates (x-mu)T.Sigma-1.(x-mu)
    exponent = np.einsum('...k,kl,...l->...', pos - mean, np.linalg.inv(cov), pos - mean)
    return np.exp(-exponent / 2) / norm_const


def find_best_w(initial_w, a_points, b_points, ratio_function):
    max_ratio = 0
    all_ratios = []
    best_w = initial_w
    all_directions = np.linspace(0, 180, 180)
    for theta in all_directions:
        theta = math.radians(theta)
        rotation = np.array([[math.cos(theta), -math.sin(theta)], [math.sin(theta), math.cos(theta)]])
        w_theta = rotation.dot(initial_w)
        projected_a = project_1d(a_points, w_theta.T)
        projected_b = project_1d(b_points, w_theta.T)
        ratio = ratio_function(projected_a, projected_b)
        all_ratios.append(ratio)
        if ratio > max_ratio:
            max_ratio = ratio
            best_w = w_theta
    return all_ratios, all_directions, best_w

'''

Part 3.I

'''

# Generate 2D Data from two Gaussians
number_a = 500
number_b = 500

# Class 'a' mean and covariance
mean_a = np.array([2, 5]).T
cov_a = np.array([[1, -1],
                  [-1, 2]])

a_data = np.random.multivariate_normal(mean_a, cov_a, number_a).T

# Class 'b' mean and covariance
mean_b = np.array([4, 7]).T
cov_b = np.array([[1, -1],
                  [-1, 2]])

b_data = np.random.multivariate_normal(mean_b, cov_b, number_b).T

fig, ax = plt.subplots(nrows=1, ncols=4)

ax[0].scatter(a_data[0], a_data[1], color='r')
ax[0].scatter(b_data[0], b_data[1], color='b')
ax[0].set_title("Original Data Points")

# 1)
# a) 3 Illustrative choices for the direction w, histograms of the values of each class
illustrative_ws = [[1, 1], [0, 1], [-1, 1]]
for index, il_w in enumerate(illustrative_ws):
    proj_a = project_1d(a_data, np.array(il_w).T)
    proj_b = project_1d(b_data, np.array(il_w).T)
    ax[index + 1].set_title("Vector Direction: {}".format(il_w))
    ax[index + 1].hist(proj_a, color='r', alpha=0.4, bins=20)
    ax[index + 1].hist(proj_b, color='b', alpha=0.4, bins=20)
plt.show()


# b) Plot dependence of F(w) by rotating a starting weight vector w(0) by angles θ to get w(θ) = R(θ)w(0)
w0 = np.array([-1, 1]).T  # Initial random direction w
ratios, directions, max_w = find_best_w(w0, a_data, b_data, fishers_ratio)
print("Best W: {}".format(max_w))

# Plot Fishers Ratio dep. on Direction of W
plt.title("Fishers Ratio dependence on direction w")
plt.plot(directions, ratios)
plt.xlabel("Direction")
plt.ylabel("F. Ratio")
plt.show()


# 2)
# a) Contour Lines
X = np.linspace(-2, 10, 100)
Y = np.linspace(-2, 10, 100)
X, Y = np.meshgrid(X, Y)
# Pack X and Y into a single 3-dimensional array
pack = np.empty(X.shape + (2,))
pack[:, :, 0] = X
pack[:, :, 1] = Y

# Plot the contours and the data points
Z_a = gaussian_2d(pack, cov_a, mean_a)
Z_b = gaussian_2d(pack, cov_b, mean_b)

plt.contour(X, Y, Z_a, 12, colors='red', linestyles='dashed')
plt.contour(X, Y, Z_b, 12, colors='blue', linestyles='dashed')

# Plot the best direction W
x = np.linspace(-5, 10, 100)
y = (max_w[1] / max_w[0]) * x
plt.plot(x, y, 'k-')
plt.title("Contour plots of the distributions and Best W direction")
plt.xlim(X.min()*1.1, X.max()*1.1)
plt.ylim(Y.min()*1.1, Y.max()*1.1)
plt.show()


# 2
# b) Bayes' Theorem and log-odds
# Assume Priors are the same P(C=a) = P(C=b)

mean_a = np.array([3, 6]).T
mean_b = np.array([7, 2]).T
positions = np.vstack([X.ravel(), Y.ravel()])

# For equal Sigma values
cov_a = np.array([[2, 1],
                  [1, 3]])
cov_b = cov_a
cov_a_inv = np.linalg.inv(cov_a)

diff_means_sigma = (mean_a - mean_b).T.dot(cov_a_inv)
i_term = - 0.5 * (mean_a.T.dot(cov_a_inv).dot(mean_a) - mean_b.T.dot(cov_a_inv).dot(mean_b))

# Plot decision boundary
log_odds = (diff_means_sigma.dot(positions) + i_term).reshape((100, 100))

plt.contour(X, Y, log_odds, levels=[0])

# Plot the contours as well
Z_a = gaussian_2d(pack, cov_a, mean_a)
Z_b = gaussian_2d(pack, cov_b, mean_b)
plt.contour(X, Y, Z_a, 12, colors='red', linestyles='dashed')
plt.contour(X, Y, Z_b, 12, colors='blue', linestyles='dashed')

plt.xlim(X.min()*1.1, X.max()*1.1)
plt.ylim(Y.min()*1.1, Y.max()*1.1)
plt.title("Decision boundary for equal covariance distributions")
plt.show()

# For different Sigma values
cov_a = np.array([[0.5, 0.2],
                  [0.2, 2]])
cov_b = np.array([[2, 0],
                  [0, 2]])

cov_a_inv = np.linalg.inv(cov_a)
cov_b_inv = np.linalg.inv(cov_b)

# Plot decision boundary
log_covariances = 0.5 * np.log(np.linalg.det(cov_a_inv) / np.linalg.det(cov_b_inv))

a_values = []
b_values = []
for p in positions.T:
    a = p.T.dot(cov_a_inv).dot(p) - (2 * (mean_a.T.dot(cov_a_inv).dot(p))) + mean_a.T.dot(cov_a_inv).dot(mean_a)
    b = p.T.dot(cov_b_inv).dot(p) - (2 * (mean_b.T.dot(cov_b_inv).dot(p))) + mean_b.T.dot(cov_b_inv).dot(mean_b)
    a_values.append(a)
    b_values.append(b)

a_values = np.array(a_values)
b_values = np.array(b_values)

values = (a_values - b_values).reshape((100, 100))

log_odds = log_covariances - (0.5 * values)
plt.contour(X, Y, log_odds, levels=[0])

# Plot the contours as well
Z_a = gaussian_2d(pack, cov_a, mean_a)
Z_b = gaussian_2d(pack, cov_b, mean_b)
plt.contour(X, Y, Z_a, 12, colors='red', linestyles='dashed')
plt.contour(X, Y, Z_b, 12, colors='blue', linestyles='dashed')

plt.xlim(X.min()*1.1, X.max()*1.1)
plt.ylim(Y.min()*1.1, Y.max()*1.1)
plt.title("Decision boundary for unequal covariance distributions")
plt.show()

# 2)
# c) Unbalanced Fishers Ratio

# First generate different amount of data points for the same original distributions
number_a = 5000
number_b = 200

mean_a = np.array([2, 5]).T
cov_a = np.array([[1, -1],
                  [-1, 2]])

# Class 'b' mean and covariance
mean_b = np.array([4, 7]).T
cov_b = np.array([[3, 1],
                  [1, 2]])

a_data = np.random.multivariate_normal(mean_a, cov_a, number_a).T

b_data = np.random.multivariate_normal(mean_b, cov_b, number_b).T

w0 = np.array([-2, 2])
_, _, unbalanced_w = find_best_w(w0, a_data, b_data, unbalanced_fishers_ratio)
_, _, balanced_w = find_best_w(w0, a_data, b_data, fishers_ratio)

print("Unbalanced {}, Balanced {}".format(unbalanced_w, balanced_w))

# Plot the differences
_, ax = plt.subplots(nrows=1, ncols=3)
proj_a = project_1d(a_data, np.array(balanced_w).T)
proj_b = project_1d(b_data, np.array(balanced_w).T)
ax[0].set_title("Balanced W separation for w: {}".format(balanced_w))
ax[0].hist(proj_a, color='r', alpha=0.4, bins=20)
ax[0].hist(proj_b, color='b', alpha=0.4, bins=20)

proj_a = project_1d(a_data, np.array(unbalanced_w).T)
proj_b = project_1d(b_data, np.array(unbalanced_w).T)
ax[1].set_title("Unbalanced W separation for w: {}".format(unbalanced_w))
ax[1].hist(proj_a, color='r', alpha=0.4, bins=20)
ax[1].hist(proj_b, color='b', alpha=0.4, bins=20)

x = np.linspace(-5, 10, 100)
y_unb = (unbalanced_w[1] / unbalanced_w[0]) * x
y_bal = (balanced_w[1] / balanced_w[0]) * x
ax[2].plot(x, y_unb, 'k--', label='Unbalanced')
ax[2].plot(x, y_bal, 'k-', label='Balanced')
ax[2].scatter(a_data[0], a_data[1], color='r', alpha=0.2)
ax[2].scatter(b_data[0], b_data[1], color='b', alpha=0.2)
ax[2].legend()
plt.show()
